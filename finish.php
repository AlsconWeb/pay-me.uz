<?php
/**
 * Template Name: Finish.
 *
 * @package iwp/payme
 */

$count = ! empty( $_COOKIE['pm_finish_count'] ) ? filter_var( wp_unslash( $_COOKIE['pm_finish_count'] ), FILTER_SANITIZE_NUMBER_INT ) : 0;

if ( empty( $count ) && isset( $_SERVER['HTTP_USER_AGENT'] ) && ! preg_match( '/bot|crawl|slurp|spider|facebookexternalhit/i', $_SERVER['HTTP_USER_AGENT'] ) ) {
	wp_redirect( get_bloginfo( 'url' ), 301 );
	die();
}

get_header();

if ( have_posts() ) {

	while ( have_posts() ) {

		the_post();
		?>
		<section class="finish">
			<div class="container-xl">
				<div class="row">
					<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<h1><?php echo esc_html( carbon_get_the_post_meta( 'pm_finish_title' ) ); ?></h1>
						<?php
						if ( (int) $count <= 4 ) {
							echo wp_kses_post( wpautop( carbon_get_the_post_meta( 'pm_finish_text' ) ) );
						} else {
							echo wp_kses_post( wpautop( carbon_get_the_post_meta( 'pm_finish_text_good' ) ) );
						}
						$domain = urlencode( get_bloginfo( 'url' ) . '/' . get_the_title() );
						?>
						<div class="soc">
							<div class="addtoany_shortcode test-class">
								<div class="a2a_kit a2a_kit_size_32 addtoany_list" style="line-height: 32px;">
									<a
											href="https://www.facebook.com/sharer/sharer.php?u=<?php echo esc_attr( $domain ); ?>"
											title="Facebook"
											rel="nofollow noopener"
											target="_blank">
										<span
												class="a2a_svg a2a_s__default a2a_s_facebook"
												style="background-color: transparent;">
											<svg
													focusable="false"
													aria-hidden="true"
													xmlns="http://www.w3.org/2000/svg"
													viewBox="0 0 32 32"><path
														fill="#FFF"
														d="M17.78 27.5V17.008h3.522l.527-4.09h-4.05v-2.61c0-1.182.33-1.99 2.023-1.99h2.166V4.66c-.375-.05-1.66-.16-3.155-.16-3.123 0-5.26 1.905-5.26 5.405v3.016h-3.53v4.09h3.53V27.5h4.223z"></path></svg></span><span
												class="a2a_label">Facebook</span>
									</a>
									<?php
									$shear_text = strip_tags( carbon_get_the_post_meta( 'pm_finish_shear_to_tg_text' ) );
									?>
									<a
											href="https://telegram.me/share/url?url=<?php echo esc_attr( $domain ); ?>&text=<?php echo esc_attr( $shear_text ); ?>"
											title="Telegram"
											rel="nofollow noopener"
											target="_blank">
										<span
												class="a2a_svg a2a_s__default a2a_s_telegram"
												style="background-color: transparent;">
											<svg
													focusable="false"
													aria-hidden="true"
													xmlns="http://www.w3.org/2000/svg"
													viewBox="0 0 32 32"><path
														fill="#FFF"
														d="M25.515 6.896 6.027 14.41c-1.33.534-1.322 1.276-.243 1.606l5 1.56 1.72 5.66c.226.625.115.873.77.873.506 0 .73-.235 1.012-.51l2.43-2.363 5.056 3.734c.93.514 1.602.25 1.834-.863l3.32-15.638c.338-1.363-.52-1.98-1.41-1.577z"></path></svg></span><span
												class="a2a_label">Telegram</span>
									</a>
								</div>
							</div>
						</div>
						<a class="button yellow" href="<?php bloginfo( 'url' ); ?>" id="new-game">
							<?php esc_html_e( 'Play again', 'pay-me' ); ?>
						</a>
						<a class="button yellow" href="#" id="shear">
							<?php esc_html_e( 'Share', 'pay-me' ); ?>
						</a>
						<p class="description-button"><?php esc_html_e( 'Share the test with your loved ones!', 'pay-me' ); ?></p>
						<p class="load">
							<?php esc_html_e( 'Be careful not to get scammed!', 'pay-me' ); ?>
						</p>
					</div>
					<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<?php
						$finish_image_id = carbon_get_the_post_meta( 'pm_finish_image' );

						if ( ! empty( $finish_image_id ) ) {
							$image_finish = wp_get_attachment_image(
								$finish_image_id,
								'full',
								null,
								[
									'class' => 'not-correct',
									'alt'   => get_the_title( $finish_image_id ),
								]
							);

							echo wp_kses_post( $image_finish );
						}
						?>
					</div>
				</div>
			</div>
		</section>
		<?php
	}
}
get_footer();

