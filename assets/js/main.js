/* global prLocalizationObj */

/**
 * @param prLocalizationObj.ajaxUrl
 * @param prLocalizationObj.actionName
 * @param prLocalizationObj.nonce
 * @param prLocalizationObj.finishUrl
 * @param prLocalizationObj.mewGame
 */

jQuery( document ).ready( function( $ ) {
	let startBt = $( '#start' );
	let allQuestionsCount = $( '[class^="question-"]' ).length;
	let rightAnswers = [];
	let answerBtn = $( '.answer-btn' );
	let nextQuestion = $( '.next' );
	let currentQuestion = 1;
	let newGageBtn = $( '#new-game' );

	// start game.
	if ( startBt.length ) {
		startBt.click( function( e ) {
			e.preventDefault();

			$( '.start' ).hide();
		} );
	}

	// Game.
	if ( answerBtn.length ) {
		answerBtn.click( function( e ) {
			e.preventDefault();

			$( this ).parents( '.row.question' ).hide();

			if ( $( this ).data( 'answer' ) === true ) {
				rightAnswers.push( 1 );
				let answerTitle = $( this ).parents( '[class^="question-"]' ).find( '.reply [id^="answer-"]' ).data( 'answer_true' );

				$( this ).parents( '[class^="question-"]' ).find( '.reply [id^="answer-"]' ).text( answerTitle );
				$( this ).parents( '[class^="question-"]' ).find( '.reply .answer-image .correct' ).show();
			} else {
				let answerTitle = $( this ).parents( '[class^="question-"]' ).find( '.reply [id^="answer-"]' ).data( 'answer_false' );

				$( this ).parents( '[class^="question-"]' ).find( '.reply [id^="answer-"]' ).text( answerTitle );
				$( this ).parents( '[class^="question-"]' ).find( '.reply .answer-image .not-correct' ).show();
			}
		} );
	}


	// next question.
	if ( nextQuestion.length ) {

		nextQuestion.click( function( e ) {
			e.preventDefault();
			if ( currentQuestion < allQuestionsCount ) {
				currentQuestion++;

			} else {

				let data = {
					action: prLocalizationObj.actionName,
					nonce: prLocalizationObj.nonce,
					countTrueAnswer: rightAnswers.length
				};

				$.ajax( {
					type: 'POST',
					url: prLocalizationObj.ajaxUrl,
					data: data,
					success: function( res ) {
						if ( res.success ) {
							setCookie( 'pm_finish_count', rightAnswers.length, 1 );
							setCookie( 'pm_count_all_questions', allQuestionsCount, 1 );
							location.href = res.data.finishUrl;
						}
					},
					error: function( xhr, ajaxOptions, thrownError ) {
						console.log( 'error...', xhr );
						//error logging
					}
				} );
			}

			$( this ).parents( '[class^="question-"]' ).hide();
		} );

	}

	// reload game.
	if ( newGageBtn.length ) {

		newGageBtn.click( function( e ) {
			setCookie( 'pm_finish_count', 0, 1 );
			setCookie( 'pm_count_all_questions', 0, 1 );
		} );

	}

	//Shear button.

	let shear = $( '#shear' );

	if ( shear.length ) {
		shear.click( function( e ) {
			e.preventDefault();

			let userAgent = navigator.userAgent;

			if ( /iPad|iPhone|iPodё|Android/.test( userAgent ) && ! window.MSStream ) {
				if ( navigator.share ) {
					navigator.share( {
						title: 'Payme',
						url: window.location.href,
					} )
						.then( () => console.log( 'Удалось поделиться' ) )
						.catch( ( error ) => console.log( 'Не удалось поделиться', error ) );
				}
			} else {
				$( '.copy-link' ).show();
				copyToClipboard( window.location.href );
				setTimeout( function() {
					$( '.copy-link' ).hide();
				}, 1500 );
			}
		} );
	}

	/**
	 * Copy in buffer.
	 *
	 *
	 * @param text
	 */
	function copyToClipboard( text ) {
		const textarea = document.createElement( 'textarea' );
		textarea.value = text;
		document.body.appendChild( textarea );
		textarea.select();
		document.execCommand( 'copy' );
		document.body.removeChild( textarea );
	}

	/**
	 * Set cookie.
	 *
	 * @param name cookie name.
	 * @param value cookie value.
	 * @param days cookie date expires.
	 */
	function setCookie( name, value, days ) {
		var expires = '';
		if ( days ) {
			var date = new Date();
			date.setTime( date.getTime() + ( days * 24 * 60 * 60 * 1000 ) );
			expires = '; expires=' + date.toUTCString();
		}
		document.cookie = name + '=' + ( value || '' ) + expires + '; path=/';
	}

	/**
	 * Get cookie.
	 *
	 * @param name cookie name.
	 * @returns {null|string}
	 */
	function getCookie( name ) {
		var nameEQ = name + '=';
		var ca = document.cookie.split( ';' );
		for ( var i = 0; i < ca.length; i++ ) {
			var c = ca[ i ];
			while ( c.charAt( 0 ) == ' ' ) c = c.substring( 1, c.length );
			if ( c.indexOf( nameEQ ) == 0 ) return c.substring( nameEQ.length, c.length );
		}
		return null;
	}
} );