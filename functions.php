<?php
/**
 * Theme functions file.
 *
 * @package iwp/payme
 */

use PayMe\Theme\Main;

require_once __DIR__ . '/vendor/autoload.php';

new Main();

