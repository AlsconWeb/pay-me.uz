<?php
/**
 * Index template file.
 *
 * @package iwp/payme
 */

get_header();

if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();

		$page_id = get_the_ID();

		$questions = carbon_get_post_meta( $page_id, 'pm_game_questions' );
		?>
		<section class="start">
			<div class="container-xl">
				<div class="row">
					<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<h1><?php echo esc_html( carbon_get_post_meta( $page_id, 'pm_home_title' ) ); ?></h1>
						<?php echo wp_kses_post( wpautop( carbon_get_post_meta( $page_id, 'pm_start_text' ) ) ); ?>
						<a class="button yellow" id="start" href="#">
							<?php echo esc_html( carbon_get_post_meta( $page_id, 'pm_home_button_text' ) ); ?>
						</a>
						<p class="load"><?php esc_html_e( 'Loading ...', 'pay-me' ); ?> <span>100</span>%</p>
					</div>
					<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<?php
						$start_image = carbon_get_the_post_meta( 'pm_home_image' );

						if ( ! empty( $start_image ) ) {

							$image = wp_get_attachment_image(
								$start_image,
								'full',
								null,
								[
									'class' => 'slot-machine',
									'alt'   => get_the_title( $start_image ),
								]
							);

							echo wp_kses_post( $image );
						}
						?>
					</div>
				</div>
			</div>
		</section>
		<?php
		if ( ! empty( $questions ) ) {
			foreach ( $questions as $key => $question ) {
				?>
				<section class="question-<?php echo esc_attr( $key ); ?>">
					<div class="container-xl">
						<div class="row question">
							<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<h1><?php echo esc_html( $question['title'] ?? '' ); ?></h1>
								<?php echo wp_kses_post( wpautop( $question['pm_questions_text'] ?? '' ) ); ?>
								<a
										class="button green answer-btn"
										data-answer="<?php echo esc_attr( $question['answer_bt_one'] ? 'true' : 'false' ); ?>"
										href="#">
									<?php echo esc_html( $question['text_bt_one'] ?? '' ); ?>
								</a>
								<a
										class="button red answer-btn"
										data-answer="<?php echo esc_attr( $question['answer_bt_two'] ? 'true' : 'false' ); ?>"
										href="#">
									<?php echo esc_html( $question['text_bt_two'] ?? '' ); ?>
								</a>
								<p class="load">
									<?php esc_html_e( 'Click the button to continue...', 'pay-me' ); ?>
								</p>
							</div>
							<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<?php
								if ( ! empty( $question['image'] ) ) {
									$image = wp_get_attachment_image(
										$question['image'],
										'full',
										null,
										[
											'alt' => get_the_title( $question['image'] ),
										]
									);

									echo wp_kses_post( $image );
								}
								?>
							</div>
						</div>
						<div class="row reply">
							<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<h1
										id="answer-<?php echo esc_attr( $key ); ?>"
										data-answer_true="<?php echo esc_html( $question['text_bt_answer_true'] ); ?>"
										data-answer_false="<?php echo esc_html( $question['text_bt_answer_false'] ); ?>">
								</h1>
								<?php echo wp_kses_post( wpautop( $question['pm_answer_text'] ) ); ?>
								<?php if ( count( $questions ) > $key + 1 ) { ?>
									<a class="button yellow next" href="#">
										<?php esc_html_e( 'Next task', 'pay-me' ); ?>
									</a>
								<?php } else { ?>
									<a class="button yellow next" href="#">
										<?php esc_html_e( 'Results', 'pay-me' ); ?>
									</a>
								<?php } ?>
								<p class="load"><?php esc_html_e( 'Click the button to continue...', 'pay-me' ); ?></p>
							</div>
							<div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-xs-12 answer-image">
								<?php
								$img_yes_id = carbon_get_the_post_meta( 'pm_questions_yes' );
								$img_no_id  = carbon_get_the_post_meta( 'pm_questions_no' );

								if ( ! empty( $img_yes_id ) ) {
									$image_yes = wp_get_attachment_image(
										$img_yes_id,
										'full',
										null,
										[
											'class' => 'hide correct',
											'alt'   => get_the_title( $img_no_id ),
										]
									);

									echo wp_kses_post( $image_yes );
								}

								if ( ! empty( $img_no_id ) ) {
									$image_no = wp_get_attachment_image(
										$img_no_id,
										'full',
										null,
										[
											'class' => 'hide not-correct',
											'alt'   => get_the_title( $img_no_id ),
										]
									);

									echo wp_kses_post( $image_no );
								}
								?>
							</div>
						</div>
					</div>
				</section>
				<?php
			}
		}
	}
}

get_footer();
