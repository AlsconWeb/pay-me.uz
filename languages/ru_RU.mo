��    *      l  ;   �      �     �     �     �     �     �               %     1     Q     g     u     �     �     �     �     �     �  
   �     �     �     �                !     -  	   9  
   C     N     ]     i     q     }  $   �     �     �  
   �  5   �                  �  !  7   �     	  1   !  ]   S     �      �     �     		  ?   )	  -   i	  !   �	     �	     �	  /   �	     
     7
     C
     L
     j
     }
  3   �
  /   �
  .   �
     '  #   A     e  !   y     �     �     �     �             �   3  5   0  
   f     q  P   �     �  $   �                               )          
         	                            '                    (       $                                            "      %   &   #                 !             *            Answer false title Answer text Answer true title Be careful not to get scammed! Button text Button text one Button text two Change logo Click the button to continue... Correct answer or not Download logo Finish image Finish text Finish text up 5 Finish title Finish. Game Game questions Home title Image Image questions no Image questions yes Invalid nonce code Language menu Link copied Loading ... Next task Play again Questions text Remove logo Results Select Logo Share Share the test with your loved ones! Shear to tg text Start Start text The theme was created for a special project repost.uz Title Top bar menu home settings Project-Id-Version: Pay me 1.1
Report-Msgid-Bugs-To: https://wordpress.org/support/theme/payme
PO-Revision-Date: 2023-06-08 13:33+0200
Last-Translator: 
Language-Team: 
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : 2);
X-Generator: Poedit 3.0.1
X-Domain: pay-me
 Не правильный ответ заголовок Текст ответа Правильный ответ  заголовк Будьте внимательны, чтобы не попасться мошенникам! Текст кнопки Кнопка текст один Текст кнопки два Изменить логотип Нажмите кнопку, чтобы продолжить… Правильный ответ или нет Загрузить логотип Финиш картинка Финиш текст Финиш текст больше 5 балов Финиш название Финиш. Игра Игровые вопросы Заголовок Изображение Вопросов по изображению нет Вопросы по изображению да Неверный одноразовый код Языковое меню Ссылка скопирована Загрузка… Следующее задание Играть снова Текст вопросов Удалить логотип Результат Выбрать логотип Поделиться Поделившись тестом с близкими Вам людьми, Вы сокращаете возможность обмана со стороны злоумышленников. Давайте позаботимся друг о друге! Текст шеренга для телеграмма Старт Начальный текст Тема создана для специального проекта repost.uz Заголовок Меню верхней панели настройки 