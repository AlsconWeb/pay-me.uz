# Copyright (C) 2023 alexl
# This file is distributed under the GNU General Public License v2 or later.
msgid ""
msgstr ""
"Project-Id-Version: Pay me 1.1\n"
"Report-Msgid-Bugs-To: https://wordpress.org/support/theme/payme\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"POT-Creation-Date: 2023-06-05T09:29:54+00:00\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"X-Generator: WP-CLI 2.4.0\n"
"X-Domain: pay-me\n"

#. Theme Name of the theme
msgid "Pay me"
msgstr ""

#. Theme URI of the theme
msgid "https://repost.uz/"
msgstr ""

#. Description of the theme
msgid "The theme was created for a special project repost.uz"
msgstr ""

#. Author of the theme
msgid "alexl"
msgstr ""

#. Author URI of the theme
msgid "https://i-wp-dev.com"
msgstr ""

#. Template Name of the theme
msgid "Finish."
msgstr ""

#: finish.php:79
msgid "Play again"
msgstr ""

#: finish.php:82
msgid "Share"
msgstr ""

#: finish.php:84
msgid "Share the test with your loved ones!"
msgstr ""

#: finish.php:86
msgid "Be careful not to get scammed!"
msgstr ""

#: footer.php:11
msgid "Link copied"
msgstr ""

#: index.php:27
msgid "Loading ..."
msgstr ""

#: index.php:75
#: index.php:112
msgid "Click the button to continue..."
msgstr ""

#: index.php:105
msgid "Next task"
msgstr ""

#: index.php:109
msgid "Results"
msgstr ""

#: src/php/CarbonFields.php:43
#: src/php/CarbonFields.php:92
msgid "home settings"
msgstr ""

#: src/php/CarbonFields.php:46
msgid "Start"
msgstr ""

#: src/php/CarbonFields.php:48
msgid "Home title"
msgstr ""

#: src/php/CarbonFields.php:49
msgid "Button text"
msgstr ""

#: src/php/CarbonFields.php:50
msgid "Start text"
msgstr ""

#: src/php/CarbonFields.php:51
#: src/php/CarbonFields.php:62
msgid "Image"
msgstr ""

#: src/php/CarbonFields.php:52
msgid "Image questions yes"
msgstr ""

#: src/php/CarbonFields.php:53
msgid "Image questions no"
msgstr ""

#: src/php/CarbonFields.php:56
msgid "Game"
msgstr ""

#: src/php/CarbonFields.php:58
msgid "Game questions"
msgstr ""

#: src/php/CarbonFields.php:61
msgid "Title"
msgstr ""

#: src/php/CarbonFields.php:63
msgid "Questions text"
msgstr ""

#: src/php/CarbonFields.php:64
msgid "Button text one"
msgstr ""

#: src/php/CarbonFields.php:66
#: src/php/CarbonFields.php:71
msgid "Correct answer or not"
msgstr ""

#: src/php/CarbonFields.php:69
msgid "Button text two"
msgstr ""

#: src/php/CarbonFields.php:74
msgid "Answer true title"
msgstr ""

#: src/php/CarbonFields.php:76
msgid "Answer false title"
msgstr ""

#: src/php/CarbonFields.php:78
msgid "Answer text"
msgstr ""

#: src/php/CarbonFields.php:96
msgid "Finish title"
msgstr ""

#: src/php/CarbonFields.php:97
msgid "Finish text"
msgstr ""

#: src/php/CarbonFields.php:98
msgid "Finish text up 5"
msgstr ""

#: src/php/CarbonFields.php:99
msgid "Shear to tg text"
msgstr ""

#: src/php/CarbonFields.php:100
msgid "Finish image"
msgstr ""

#: src/php/Main.php:70
msgid "Top bar menu"
msgstr ""

#: src/php/Main.php:71
msgid "Language menu"
msgstr ""

#: src/php/Main.php:197
msgid "Download logo"
msgstr ""

#: src/php/Main.php:202
msgid "Select Logo"
msgstr ""

#: src/php/Main.php:203
msgid "Remove logo"
msgstr ""

#: src/php/Main.php:204
msgid "Change logo"
msgstr ""

#: src/php/Main.php:220
msgid "Invalid nonce code"
msgstr ""

#: src/php/Main.php:243
msgid "Count up"
msgstr ""

#: src/php/Statistic.php:38
msgid "Statistic open coupon"
msgstr ""

#: src/php/Statistic.php:54
msgid "Game passed"
msgstr ""
