<?php
/**
 * Header theme template.
 *
 * @package iwp/payme
 */

?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?></title>
	<link rel="preconnect" href="https://fonts.googleapis.com">
	<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
	<?php wp_head(); ?>
<body <?php body_class(); ?>>
<header>
	<div class="container-xl">
		<div class="row justify-content-between align-items-center">
			<div class="col-auto">
				<?php the_custom_logo(); ?>
				<i class="icon-close"></i>
				<a class="logo-img" href="https://repost.uz/">
					<img src="<?php echo esc_url( get_theme_mod( 'pm_logo' ) ) ?? ''; ?>" alt="">
				</a>
			</div>
			<div class="col">
				<?php
				if ( has_nav_menu( 'top_bar_menu' ) ) {
					wp_nav_menu(
						[
							'theme_location'  => 'top_bar_menu',
							'container'       => '',
							'container_class' => 'menu-primary',
							'menu_class'      => 'soc',
						]
					);
				}
				?>
			</div>
			<?php if ( has_nav_menu( 'top_bar_lang' ) ) { ?>
				<div class="col-auto">
					<?php
					do_action( 'wpml_add_language_selector' );
					?>
				</div>
			<?php } ?>
		</div>
	</div>
</header>
