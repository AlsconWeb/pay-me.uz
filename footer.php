<?php
?>
<footer>
	<div class="container-xl">
		<div class="row">
			<div class="col-12"></div>
		</div>
	</div>
</footer>
<div class="copy-link">
	<p class="icon-copy"><?php esc_html_e( 'Link copied', 'pay-me' ); ?></p>
</div>
<div id="coinsContainer">
	<img
			class="cloud-one"
			src="<?php echo esc_url( get_stylesheet_directory_uri() . '/assets/img/cloud-1.svg' ); ?>"
			alt="Cloud one">
	<img
			class="cloud-two"
			src="<?php echo esc_url( get_stylesheet_directory_uri() . '/assets/img/cloud-2.svg' ); ?>"
			alt="Cloud two">
	<img
			class="cloud-three"
			src="<?php echo esc_url( get_stylesheet_directory_uri() . '/assets/img/cloud-3.svg' ); ?>"
			alt="Cloud three">
</div>
<?php wp_footer(); ?>
</body>
</html>

