<?php
/**
 * Statistic done game widget.
 *
 * @package iwp/payme
 */

namespace PayMe\Theme;

/**
 * Statistic class file.
 */
class Statistic {
	/**
	 * Statistic construct.
	 */
	public function __construct() {
		$this->init();
	}

	/**
	 * Init hooks and actions.
	 *
	 * @return void
	 */
	private function init(): void {
		add_action( 'wp_dashboard_setup', [ $this, 'register_widget' ] );
	}

	/**
	 * Show widget
	 *
	 * @return void
	 */
	public function register_widget(): void {
		wp_add_dashboard_widget(
			'pra_statistic_game',
			__( 'Statistic open coupon', 'pay-me' ),
			[ $this, 'dashboard_widget_statistic_handler' ],
			[ $this, 'dashboard_widget_statistic_config_handler' ]
		);
	}

	/**
	 * Statistic handler.
	 *
	 * @return void
	 */
	public function dashboard_widget_statistic_handler(): void {
		$count = get_option( 'pm_game_count', false ) ?: 0;

		printf(
			'<div class="count"><p><b>%s</b> : %d</p></div>',
			esc_html( __( 'Game passed', 'pay-me' ) ),
			esc_html( $count )
		);
	}

	public function dashboard_widget_statistic_config_handler(): void {

	}

}
