<?php

/**
 * Main theme class.
 *
 * @package iwp/payme
 */

namespace PayMe\Theme;

use WP_Customize_Image_Control;

/**
 * Main class file.
 */
class Main {
	public const THEME_VERSION = '1.2.0';

	/**
	 * Game complete action and nonce code.
	 */
	public const PM_GAME_COMPLETED_ACTION_NAME = 'pm_game_complete';

	/**
	 * Main construct.
	 */
	public function __construct() {
		$this->init();

		new CarbonFields();
		new Statistic();
	}

	/**
	 * Init hooks and actions.
	 *
	 * @return void
	 */
	private function init(): void {

		// actions.
		add_action( 'after_setup_theme', [ $this, 'add_theme_support' ] );
		add_action( 'wp_enqueue_scripts', [ $this, 'add_scripts' ] );
		add_action( 'customize_register', [ $this, 'add_two_logo' ] );

		// filters.
		add_filter( 'mime_types', [ $this, 'add_support_mimes' ] );
		add_filter( 'get_custom_logo', [ $this, 'output_logo' ] );

		// ajax game done.
		add_action( 'wp_ajax_' . self::PM_GAME_COMPLETED_ACTION_NAME, [ $this, 'game_statistic_count' ] );
		add_action( 'wp_ajax_nopriv_' . self::PM_GAME_COMPLETED_ACTION_NAME, [ $this, 'game_statistic_count' ] );
	}

	/**
	 * Theme support.
	 *
	 * @return void
	 */
	public function add_theme_support(): void {

		// add custom logo.
		add_theme_support( 'custom-logo' );

		// add menu support.
		add_theme_support( 'menus' );

		register_nav_menus(
			[
				'top_bar_menu' => __( 'Top bar menu', 'pay-me' ),
				'top_bar_lang' => __( 'Language menu', 'pay-me' ),
			]
		);

		load_theme_textdomain( 'pay-me', get_template_directory() . '/languages' );
	}

	/**
	 * Add scripts and style.
	 *
	 * @return void
	 */
	public function add_scripts(): void {

		$url = get_stylesheet_directory_uri();
		$min = '.min';

		if ( defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ) {
			$min = '';
		}

		wp_enqueue_script(
			'pm_build',
			$url . '/assets/js/build' . $min . '.js',
			[
				'jquery',

			],
			self::THEME_VERSION,
			true
		);

		wp_enqueue_script(
			'pm_main',
			$url . '/assets/js/main' . $min . '.js',
			[
				'jquery',
			],
			self::THEME_VERSION,
			true
		);

		wp_localize_script(
			'pm_main',
			'prLocalizationObj',
			[
				'ajaxUrl'    => admin_url( 'admin-ajax.php' ),
				'nonce'      => wp_create_nonce( self::PM_GAME_COMPLETED_ACTION_NAME ),
				'actionName' => self::PM_GAME_COMPLETED_ACTION_NAME,
				'mewGame'    => get_bloginfo( 'url' ),
			]
		);

		wp_enqueue_script( 'html5shiv', '//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js', [], '3.7.0', false );
		wp_enqueue_script( 'respond', '//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js', [], '1.4.2', false );
		wp_script_add_data( 'html5shiv', 'conditional', 'lt IE 9' );
		wp_script_add_data( 'respond', 'conditional', 'lt IE 9' );

		wp_enqueue_style( 'google-font', 'https://fonts.googleapis.com/css2?family=Press+Start+2P&amp;display=swap', '', self::THEME_VERSION );
		wp_enqueue_style( 'main', $url . '/assets/css/main' . $min . '.css', '', self::THEME_VERSION );
		wp_enqueue_style( 'style', $url . '/style.css', '', self::THEME_VERSION );
	}

	/**
	 * Add SVG and Webp formats to upload.
	 *
	 * @param array $mimes Mimes type.
	 *
	 * @return array
	 */
	public function add_support_mimes( array $mimes ): array {

		$mimes['webp'] = 'image/webp';
		$mimes['svg']  = 'image/svg+xml';

		return $mimes;
	}

	/**
	 * Change output custom logo.
	 *
	 * @param string $html HTML custom logo.
	 *
	 * @return string
	 */
	public function output_logo( string $html ): string {
		$class = 'logo-img';
		if ( has_custom_logo() ) {
			$white_logo = get_theme_mod( 'pm_logo' );
			$logo       = wp_get_attachment_image(
				get_theme_mod( 'custom_logo' ),
				'full',
				false,
				[
					'class'    => 'logo-img',
					'itemprop' => 'logo',
				]
			);
			$content    = $logo;

			$html = sprintf(
				'<a href="%s" class="%s" rel="home" itemprop="url">%s</a>',
				'https://blog.payme.uz',
				$class,
				$content
			);

		}

		return $html;
	}

	/**
	 * Add White logo.
	 *
	 * @param $wp_customize
	 *
	 * @return void
	 */
	public function add_two_logo( $wp_customize ): void {
		$wp_customize->add_setting( 'pm_logo' );

		$wp_customize->add_control(
			new WP_Customize_Image_Control(
				$wp_customize,
				'pm_logo_control',
				[
					'label'         => __( 'Download logo', 'pay-me' ),
					'priority'      => 20,
					'section'       => 'title_tagline',
					'settings'      => 'pm_logo',
					'button_labels' => [
						'select' => __( 'Select Logo', 'pay-me' ),
						'remove' => __( 'Remove logo', 'pay-me' ),
						'change' => __( 'Change logo', 'pay-me' ),
					],
				]
			)
		);
	}

	/**
	 * Game statistic count.
	 *
	 * @return void
	 */
	public function game_statistic_count(): void {
		$nonce = ! empty( $_POST['nonce'] ) ? filter_var( wp_unslash( $_POST['nonce'] ), FILTER_SANITIZE_STRING ) : '';

		if ( ! wp_verify_nonce( $nonce, self::PM_GAME_COMPLETED_ACTION_NAME ) ) {
			wp_send_json_error( [ 'message' => __( 'Invalid nonce code', 'pay-me' ) ] );
		}

		$count_true_answer = ! empty( $_POST['countTrueAnswer'] ) ? filter_var( wp_unslash( $_POST['countTrueAnswer'] ), FILTER_SANITIZE_NUMBER_INT ) : 1;

		$current_language = apply_filters( 'wpml_current_language', null );

		if ( 'uz' === $current_language ) {
			$finish_page_url = get_bloginfo( 'url' ) . '/' . $current_language . '/level-' . $count_true_answer;
		} else {
			$finish_page_url = get_bloginfo( 'url' ) . '/level-' . $count_true_answer;
		}

		$count = get_option( 'pm_game_count', false );

		if ( ! $count ) {
			add_option( 'pm_game_count', 1 );
		} else {
			update_option( 'pm_game_count', ++ $count, true );
		}

		wp_send_json_success(
			[
				'message'   => __( 'Count up', 'pay-me' ),
				'finishUrl' => $finish_page_url,
			]
		);
	}

}
