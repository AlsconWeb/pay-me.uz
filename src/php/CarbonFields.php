<?php
/**
 * Create carbon fields from theme.
 *
 * @package iwp/payme
 */

namespace PayMe\Theme;

use Carbon_Fields\Carbon_Fields;
use Carbon_Fields\Container;
use Carbon_Fields\Field;

/**
 * CarbonFields class file.
 */
class CarbonFields {
	/**
	 * CarbonFields construct.
	 */
	public function __construct() {
		add_action( 'after_setup_theme', [ $this, 'carbon_fields_load' ] );
		add_action( 'carbon_fields_register_fields', [ $this, 'add_carbon_fields_game' ] );
		add_action( 'carbon_fields_register_fields', [ $this, 'add_carbon_fields_finish' ] );
		add_action( 'carbon_fields_register_fields', [ $this, 'add_theme_options_page' ] );
	}

	/**
	 * Load carbon field.
	 *
	 * @return void
	 */
	public function carbon_fields_load(): void {
		Carbon_Fields::boot();
	}

	/**
	 * Add carbon fields to courses CPT.
	 *
	 * @return void
	 */
	public function add_carbon_fields_game(): void {
		Container::make( 'post_meta', __( 'home settings', 'pay-me' ) )
			->where( 'post_id', '=', get_option( 'page_on_front' ) )
			->add_tab(
				__( 'Start', 'pay-me' ),
				[
					Field::make( 'text', 'pm_home_title', __( 'Home title', 'pay-me' ) )->set_width( 50 ),
					Field::make( 'text', 'pm_home_button_text', __( 'Button text', 'pay-me' ) )->set_width( 50 ),
					Field::make( 'rich_text', 'pm_start_text', __( 'Start text', 'pay-me' ) ),
					Field::make( 'image', 'pm_home_image', __( 'Image', 'pay-me' ) )->set_width( 30 ),
					Field::make( 'image', 'pm_questions_yes', __( 'Image questions yes', 'pay-me' ) )->set_width( 30 ),
					Field::make( 'image', 'pm_questions_no', __( 'Image questions no', 'pay-me' ) )->set_width( 30 ),
				]
			)->add_tab(
				__( 'Game', 'pay-me' ),
				[
					Field::make( 'complex', 'pm_game_questions', __( 'Game questions', 'pay-me' ) )
						->add_fields(
							[
								Field::make( 'text', 'title', __( 'Title', 'pay-me' ) )->set_width( 50 ),
								Field::make( 'image', 'image', __( 'Image', 'pay-me' ) )->set_width( 50 ),
								Field::make( 'rich_text', 'pm_questions_text', __( 'Questions text', 'pay-me' ) ),
								Field::make( 'text', 'text_bt_one', __( 'Button text one', 'pay-me' ) )
									->set_width( 70 ),
								Field::make( 'checkbox', 'answer_bt_one', __( 'Correct answer or not', 'pay-me' ) )
									->set_option_value( 'true' )
									->set_width( 30 ),
								Field::make( 'text', 'text_bt_two', __( 'Button text two', 'pay-me' ) )
									->set_width( 70 ),
								Field::make( 'checkbox', 'answer_bt_two', __( 'Correct answer or not', 'pay-me' ) )
									->set_option_value( 'true' )
									->set_width( 30 ),
								Field::make( 'text', 'text_bt_answer_true', __( 'Answer true title', 'pay-me' ) )
									->set_width( 50 ),
								Field::make( 'text', 'text_bt_answer_false', __( 'Answer false title', 'pay-me' ) )
									->set_width( 50 ),
								Field::make( 'rich_text', 'pm_answer_text', __( 'Answer text', 'pay-me' ) ),
							]
						),
				]
			);
	}

	/**
	 * Add carbon field in finish page.
	 *
	 * @return void
	 */
	public function add_carbon_fields_finish(): void {

		Container::make( 'post_meta', __( 'home settings', 'pay-me' ) )
			->where( 'post_template', '=', 'finish.php' )
			->add_fields(
				[
					Field::make( 'text', 'pm_finish_title', __( 'Finish title', 'pay-me' ) ),
					Field::make( 'rich_text', 'pm_finish_text', __( 'Finish text', 'pay-me' ) ),
					Field::make( 'rich_text', 'pm_finish_text_good', __( 'Finish text up 5', 'pay-me' ) ),
					Field::make( 'rich_text', 'pm_finish_shear_to_tg_text', __( 'Shear to tg text', 'pay-me' ) ),
					Field::make( 'image', 'pm_finish_image', __( 'Finish image', 'pay-me' ) )->set_width( 50 ),
				]
			);
	}

	/**
	 * Add theme options page.
	 *
	 * @return void
	 */
	public function add_theme_options_page(): void {

		$basic_options_container = Container::make( 'theme_options', __( 'Налаштування теми', 'alevel' ) )
			->set_page_file( 'theme-options' )
			->add_fields(
				[
					Field::make( 'header_scripts', 'pm_header_script', __( 'Script у шапці', 'alevel' ) )
						->set_hook( 'wp_print_scripts', 20 ),
					Field::make( 'footer_scripts', 'pm_footer_script', __( 'Script у підвалі', 'alevel' ) ),
					Field::make( 'textarea', 'pm_footer_text', __( 'Текст під логотипом', 'alevel' ) )
						->set_rows( 4 ),
				]
			);
	}
}
